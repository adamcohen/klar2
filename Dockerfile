# Copyright 2017 clair authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM golang:1.10-alpine

EXPOSE 6060 6061

ARG CLAIR_REPO_TAG
ENV CLAIR_REPO_TAG ${CLAIR_REPO_TAG:-v2.0.9}

ARG KLAR_EXECUTABLE_VERSION
ENV KLAR_EXECUTABLE_VERSION ${KLAR_EXECUTABLE_VERSION:-2.4.0}

ARG KLAR_EXECUTABLE_SHA
ENV KLAR_EXECUTABLE_SHA ${KLAR_EXECUTABLE_SHA:-09764983d4e9a883754b55b16edf5f0be558ab053ad6ee447aca0199ced3d09f}

WORKDIR /go/src/github.com/coreos/clair/
RUN apk add --no-cache git rpm xz dumb-init git supervisor wget ca-certificates nodejs npm && \
    git clone --branch $CLAIR_REPO_TAG https://github.com/coreos/clair /go/src/github.com/coreos/clair/ && \
    wget https://github.com/optiopay/klar/releases/download/v${KLAR_EXECUTABLE_VERSION}/klar-${KLAR_EXECUTABLE_VERSION}-linux-amd64 \
      -O /klar-${KLAR_EXECUTABLE_VERSION} && \
    echo "${KLAR_EXECUTABLE_SHA}  /klar-${KLAR_EXECUTABLE_VERSION}" | sha256sum -c && \
    chmod +x /klar-${KLAR_EXECUTABLE_VERSION} && \
    export CLAIR_VERSION=$(git describe --always --tags --dirty) && \
    go install -ldflags "-X github.com/coreos/clair/pkg/version.Version=$CLAIR_VERSION" -v github.com/coreos/clair/cmd/clair && \
    mv /go/bin/clair /clair && \
    rm -rf /go /usr/local/go

COPY support-files /support-files

WORKDIR /support-files/converter
RUN npm i

ENTRYPOINT ["/support-files/start.sh"]
