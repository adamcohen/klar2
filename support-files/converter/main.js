const convert = require('./convert')

try {
  convert.ProcessVulnerabilitiesFile(process.argv.slice(2));
} catch(e) {
  console.error(e.message)
  process.exit(1)
}
