const chai = require('chai')
const fs = require('fs')
const sinon = require('sinon')

const convert = require('../convert.js')
const logger = require('../logger.js')

const { expect } = chai

chai.use(require('chai-fs'));
chai.use(require('sinon-chai'))

const originalReadFileSync = fs.readFileSync

const outputReportPath = './test/tmp/gl-container-scanning-report.json'
const klarOutputFilePath = './test/fixtures/klar-output.json'
const expectedReportPath = './test/expect/gl-container-scanning-report.json'
const imageName = 'image-name-goes-here'

const inputFixture = fs.readFileSync(klarOutputFilePath, 'utf8')

afterEach(() => {
  try {
    fs.unlinkSync(outputReportPath)
  } catch(e) {
    if(e.code != 'ENOENT') {
      console.error(`Error occurred while attempting to remove temporary file ${outputReportPath}: ${e}`)
    }
  }
})

describe('#ProcessVulnerabilitiesFile', () => {
  let sandbox

  beforeEach(() => {
    sandbox = sinon.createSandbox()
    sandbox.spy(logger, 'error')
    sandbox.spy(logger, 'info')
    sandbox.spy(logger, 'log')
  })

  afterEach(() => {
    sandbox.restore()
  })

  context('when invalid parameters are provided', () => {
    context('because not enough arguments are passed', () => {
      it('throws an error', () => {
        expect(() => convert.ProcessVulnerabilitiesFile([imageName, klarOutputFilePath])).to.throw(/Error: Not enough arguments passed./)
      })
    })

    context('because the klar container scanning report file does not exist', () => {
      it('throws an error', () => {
        expect(() => convert.ProcessVulnerabilitiesFile([imageName, 'non-existent-file', outputReportPath]) )
          .to.throw(/Error encountered while attempting to read klar vulnerabilities file/)
      })
    })

    context('because the klar container scanning report does not contain JSON', () => {
      it('throws a JSON parse error', () => {
        const fileWithInvalidContentsPath = 'file-with-invalid-format'

        sandbox.stub(fs, 'readFileSync').callsFake(originalReadFileSync)
          .withArgs(fileWithInvalidContentsPath).returns('file contents here')

        expect(() => convert.ProcessVulnerabilitiesFile([imageName, fileWithInvalidContentsPath, outputReportPath]) )
          .to.throw(/JSON: SyntaxError: Unexpected token/)
      })
    })

    context('because the klar container scanning report does not contain the "Vulnerabilities" key', () => {
      it('throws an error explaining that the JSON file should have the "Vulnerabilities" key', () => {
        const fileWithInvalidJSONPath = 'file-with-invalid-json'

        sandbox.stub(fs, 'readFileSync').callsFake(originalReadFileSync)
          .withArgs(fileWithInvalidJSONPath).returns('{}')

        expect(() => convert.ProcessVulnerabilitiesFile([imageName, fileWithInvalidJSONPath, outputReportPath]) )
          .to.throw(/Expected JSON file to have key: 'Vulnerabilities'/)
      })
    })
  })

  context('when valid parameters are provided', () => {
    it('outputs the number of unapproved vulnerabilities', () => {
      convert.ProcessVulnerabilitiesFile([imageName, klarOutputFilePath, outputReportPath])
      expect(logger.error).to.have.been.calledWithMatch(/Image \[image-name-goes-here\] contains 7 unapproved vulnerabilities/)
    })

    it('produces an output file which matches the expected report', () => {
      convert.ProcessVulnerabilitiesFile([imageName, klarOutputFilePath, outputReportPath])
      expect(outputReportPath).to.be.a.file().and.equal(expectedReportPath)
    })

    context('and the klar vulnerabilities report is empty', () => {
      beforeEach(() => {
        const emptyKlarVulnerabilitiesReportName = 'empty-klar-vulnerabilities-report'

        sandbox.stub(fs, 'readFileSync').callsFake(originalReadFileSync)
          .withArgs(emptyKlarVulnerabilitiesReportName).returns('{"LayerCount":11,"Vulnerabilities":{}}')

        convert.ProcessVulnerabilitiesFile([imageName, emptyKlarVulnerabilitiesReportName, outputReportPath])
      })

      it('outputs a log message explaining there are no unapproved vulnerabilities', () => {
        expect(logger.info).to.have.been.calledWithMatch(/Image \[image-name-goes-here\] contains NO unapproved vulnerabilities/)
      })

      it('saves an empty vulnerabilities report file', () => {
        expect(outputReportPath).to.be.a.file().with.content(JSON.stringify({
          "image": "image-name-goes-here",
          "vulnerabilities": [],
          "unapproved": []
        }, null, 2))
      })
    })
  })
})
