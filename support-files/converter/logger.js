const chalk = require('chalk');

// simple wrapper to allow us to set up a spy without interfering with console.log
function output(logger, msg, level, colourFormatter) {
  if (process.env.NODE_ENV == 'test') { return }

  if (level === undefined) {
    logger(msg)
    return
  }

  const formattedLevel = level.toUpperCase().substring(0,3)
  logger(colourFormatter(`[${formattedLevel}] ▶ ${msg}`))
}

function info(msg) { output(console.info, msg, 'info', chalk.green) }
function error(msg) { output(console.error, msg, 'error', chalk.red) }
function warn(msg) { output(console.warn, msg, 'warn', chalk.yellow) }
function log(msg) { output(console.log, msg) }

module.exports = {
  info,
  error,
  warn,
  log
}
