#!/usr/bin/env sh

set -e

# only vulnerabilities with the following severity value or higher will be output
export CLAIR_OUTPUT=${CLAIR_OUTPUT:-Unknown}

# by default, we'll be using the `clair-vulnerabilities-db` service alias defined in our .gitlab-ci.yml file,
# however, it can be useful to override this value against your local postgres instance when testing locally
export CLAIR_VULNERABILITIES_DB_URL=${CLAIR_VULNERABILITIES_DB_URL:-clair-vulnerabilities-db}

export WHITELIST_FILE=${CI_PROJECT_DIR}/clair-whitelist.yaml

# we should always be running klar against the clair server running locally
export CLAIR_ADDR=localhost

# output format used by klar

export FORMAT_OUTPUT=json
# this is necessary to enable the npm script to output colour
export FORCE_COLOR=1

mkdir -p /var/log/supervisor

# allow overriding the postgres vulnerabilities database url used by clair
sed -i s/POSTGRES-VULNERABILITIES-DB-URL/${CLAIR_VULNERABILITIES_DB_URL}/g /support-files/clair/config.yaml

# workaround for this bug https://gitlab.com/gitlab-org/gitlab-runner/issues/1380
[ -f /ran.txt ] && exit 0 || >/ran.txt
/usr/bin/supervisord -c /support-files/supervisord/supervisord.conf

retries=0
echo "Waiting for clair daemon to start"
while( ! wget -T 10 -q -O /dev/null http://localhost:6060/v1/namespaces ) do
  sleep 1
  echo -n "."
  if [ $retries -eq 10 ]
  then
    echo " Timeout, aborting."
    exit 1
  fi
  retries=$(($retries+1))
done

# if the WHITELIST_FILE is set (the default behaviour), klar won't run unless the file exists, so we create an empty file here
# in case the user hasn't added one to their project.
touch ${CI_PROJECT_DIR}/clair-whitelist.yaml

echo ""
echo "Scanning container from registry '${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}' for vulnerabilities with severity level '${CLAIR_OUTPUT}' or higher."
echo ""

set +e
# scan the container and output the report using klar
/klar-${KLAR_EXECUTABLE_VERSION} ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} > ${CI_PROJECT_DIR}/klar-container-scanning-report.json

# Klar process returns if 0 if the number of detected high severity vulnerabilities in an image is less than or equal to the CLAIR_THRESHOLD
# value (default is 0) and 1 if there were more. It will return 2 if an error has prevented the image from being analyzed.  We only want to
# return an error and stop further processing if klar has returned error code 2
if [ $? -eq 2 ]
then
  echo "An error occurred while attempting to scan the container from registry '${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG}', exiting"
  exit 1
fi

set -e

# transform the klar container scanning report and write the results to a file
npm run --prefix /support-files/converter convert -- ${CI_APPLICATION_REPOSITORY}:${CI_APPLICATION_TAG} ${CI_PROJECT_DIR}/klar-container-scanning-report.json ${CI_PROJECT_DIR}/gl-container-scanning-report.json
