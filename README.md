# Vulnerability Static Analysis for Docker Containers
GitLab Analyzer for Docker Containers.

This analyzer is a wrapper around [clair](https://github.com/coreos/clair/), a vulnerability static analysis for docker containers, utilizing [klar](https://github.com/optiopay/klar) to analyze images stored in a private or public Docker registry for security vulnerabilities.

## Environment Variables used in docker image

| Variable                     | Details                                                             | Default                                                          |
| ------                       | ------                                                              | ------                                                           |
| CLAIR_REPO_TAG               | git tag of the [clair](https://github.com/coreos/clair/) repository | v2.0.9                                                           |
| KLAR_EXECUTABLE_VERSION      | version of the [klar](https://github.com/optiopay/klar) binary      | 2.4.0                                                            |
| KLAR_EXECUTABLE_SHA          | expected SHA 256 sum for the klar binary                            | 09764983d4e9a883754b55b16edf5f0be558ab053ad6ee447aca0199ced3d09f |
| CLAIR_OUTPUT                 | minimum clair severity level to output                              | Unknown                                                          |
| REGISTRY_INSECURE            | should only be set to `true` when testing the image locally         | false                                                            |
| CLAIR_VULNERABILITIES_DB_URL | should only be configured when testing the image locally            | clair-vulnerabilities-db                                         |

## Versioning and release process

Please check the common [Versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see
the [LICENSE](LICENSE) file.
