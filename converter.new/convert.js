const fs = require('fs')
const logger = require('./logger')

const {table} = require('table');

const descriptionColumnWidth = 62

const vulnerabilityHeaders = {
  status: 'STATUS',
  severity: 'CVE SEVERITY',
  packageName: 'PACKAGE NAME',
  packageVersion: 'PACKAGE VERSION',
  cveDescription: 'CVE DESCRIPTION'
}

function ProcessVulnerabilitiesFile(args) {
  if (args.length < 3) {
    throw new Error('Error: Not enough arguments passed. Usage: node main.js [image-file-name] [json-input-file-name] [output-file-name]')
  }

  const dockerImageName = args[0]
  const klarContainerScanningReportFile = args[1]
  const glContainerScanningReportFile = args[2]

  let klarVulnerabilitiesFileContents
  try {
    klarVulnerabilitiesFileContents = fs.readFileSync(klarContainerScanningReportFile, 'utf8')
  } catch(e) {
    throw new Error(`Error encountered while attempting to read klar vulnerabilities file with path '${klarContainerScanningReportFile}': ${e}`)
  }

  const convertedVulnerabilitiesJSON = Convert(dockerImageName, klarVulnerabilitiesFileContents)

  // report vulnerabilities
  reportToConsole(convertedVulnerabilitiesJSON)
  reportToFile(glContainerScanningReportFile, convertedVulnerabilitiesJSON)
}

function reportToConsole(convertedVulnerabilitiesJSON) {
  const vulnerabilitiesTableOutput = vulnerabilitiesToTable(convertedVulnerabilitiesJSON.vulnerabilities)

  const numberOfUnapprovedVulnerabilities = convertedVulnerabilitiesJSON.unapproved.length

  if (numberOfUnapprovedVulnerabilities === 0) {
    logger.info(`[INFO] ▶ Image [${convertedVulnerabilitiesJSON.image}] contains NO unapproved vulnerabilities`);
    return
  }

  logger.error(`[ERRO] ▶ Image [${convertedVulnerabilitiesJSON.image}] contains ${numberOfUnapprovedVulnerabilities} unapproved vulnerabilities`)

  logger.log(vulnerabilitiesTableOutput);
}

function reportToFile(pathToFile, convertedVulnerabilitiesJSON) {
  try {
    fs.writeFileSync(pathToFile, JSON.stringify(convertedVulnerabilitiesJSON, null, 2))
  } catch(e) {
    throw new Error(`Error encountered while attempting to write vulnerabilities report to file with path '${pathToFile}': ${e}`)
  }
}

function emptyVulnerabilities(klarVulnerabilitiesJSON) {
  for(var key in klarVulnerabilitiesJSON) {
    if(klarVulnerabilitiesJSON.hasOwnProperty(key))
      return false
  }

  return true
}


function Convert(dockerImageName, inputString) {
  var klarVulnerabilitiesJSON
  var vulnerabilities = []
  var unapprovedVulnerabilities = []

  try {
    klarVulnerabilitiesJSON = JSON.parse(inputString)
  } catch(e) {
    throw new Error(`Error encountered while attempting to convert klar vulnerabilities file into JSON: ${e}`)
  }

  if (emptyVulnerabilities(klarVulnerabilitiesJSON)) {
    throw new Error(`Error encountered while attempting to parse klar vulnerabilities file: Expected JSON file to have key: 'Vulnerabilities'`)
  }

  Object.entries(klarVulnerabilitiesJSON.Vulnerabilities).forEach(([_, vulnerabilitiesForLevel]) => {
    for (const vulnerability of vulnerabilitiesForLevel) {
      const transformedVulnerability = {
        fixedby: vulnerability.FixedBy,
        severity: vulnerability.Severity,
        link: vulnerability.Link,
        description: vulnerability.Description,
        namespace: vulnerability.NamespaceName,
        vulnerability: vulnerability.Name,
        featureversion: vulnerability.FeatureVersion,
        featurename: vulnerability.FeatureName
      }

      vulnerabilities.push(transformedVulnerability)
    }
  })

  const sortedVulnerabilities = vulnerabilities.sort((a, b) => a.vulnerability.localeCompare(b.vulnerability))

  const vulnerabilitiesJSON = {
    image: dockerImageName,
    vulnerabilities: sortedVulnerabilities,
    unapproved: sortedVulnerabilities.map(x => x.vulnerability)
  }

  return vulnerabilitiesJSON
}

function vulnerabilitiesToTable(vulnerabilities) {
  const tableConfig = {
    columns: {
      4: {
        width: descriptionColumnWidth,
        wrapWord: true
      }
    }
  };

  let outData = [
    [
      vulnerabilityHeaders.status, vulnerabilityHeaders.severity, vulnerabilityHeaders.packageName,
      vulnerabilityHeaders.packageVersion, vulnerabilityHeaders.cveDescription
    ]
  ]

  for (const vulnerability of vulnerabilities) {
    outData.push(
      [
        "Unapproved",
        `${vulnerability.severity} ${vulnerability.vulnerability}`,
        vulnerability.featurename,
        vulnerability.featureversion,
        vulnerability.description
      ])
  }

  return table(outData, tableConfig);
}

module.exports = {
  ProcessVulnerabilitiesFile
}
