const chalk = require('chalk');

// simple wrapper to allow us to set up a spy without interfering with console.log
function output(logger, msg) {
  if (process.env.NODE_ENV == 'test') { return }
  logger(msg)
}

function info(msg) { output(console.info, chalk.green(msg)) }
function error(msg) { output(console.error, chalk.red(msg)) }
function warn(msg) { output(console.warn, chalk.yellow(msg)) }
function log(msg) { output(console.log, chalk.white(msg)) }

module.exports = {
  info,
  error,
  warn,
  log
}
